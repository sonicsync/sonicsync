# SonicSync

## Building

Building the project should mostly just be running `cargo build`, but if said command is failing you might want to try one of the following: 

- Ensure that you have all the requirements for gkt-rs installed: http://gtk-rs.org/docs/requirements.html
- Update your Rust toolchain