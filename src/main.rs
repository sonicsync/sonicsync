extern crate gtk;
extern crate glib;

use std;
use std::sync::Arc;
use std::cell::RefCell;
use gtk::prelude::*;

use std::env::args;

mod api;

mod album;
use album::Album;

mod main_window;
use main_window::MainWindow;

fn main() {
    gtk::init().expect("Unable to start GTK3. Error");

    let gui = Arc::new(MainWindow::new());

    gui.start();
    gtk::main();
}