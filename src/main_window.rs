use gtk::prelude::*;

pub struct MainWindow {
    window: gtk::ApplicationWindow,
    menu: gtk::MenuBar,
}

impl MainWindow {
    pub fn new() -> MainWindow {
        let glade_src = include_str!("application.glade");
        let builder = gtk::Builder::new_from_string(glade_src);

        let window: gtk::ApplicationWindow = builder.get_object("mainwindow").unwrap();
        let menu: gtk::MenuBar = builder.get_object("toolbar").unwrap();

        MainWindow {
            window,
            menu,
        }
    }

    pub fn start(&self) {
        glib::set_application_name("SonicSync");
        self.window.set_wmclass("SonicSync", "SonicSync");
        self.window.connect_delete_event(|_, _| { gtk::main_quit(); Inhibit(false) });
        self.window.show_all();
    }

//    pub fn display_album(album: Album) {
//        println!("(:?)", album);
//    }
}

