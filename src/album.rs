pub struct Album {
    title: String,
    artist: String,
    year: u16,
}

impl Album {
    pub fn new(title: &str, album: &str, year: u16) -> Album {
        let t: &str = title;
        let a: &str = album;
        let y: u16 = year;

        Album { title: t.to_string(), artist:  a.to_string(), year: y }
    }
}